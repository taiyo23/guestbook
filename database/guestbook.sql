-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1:3306
-- Thời gian đã tạo: Th4 21, 2019 lúc 02:00 PM
-- Phiên bản máy phục vụ: 5.7.14
-- Phiên bản PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `guestbook`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_message`
--

DROP TABLE IF EXISTS `tbl_message`;
CREATE TABLE IF NOT EXISTS `tbl_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text,
  `status` tinyint(2) DEFAULT NULL,
  `visitor_name` varchar(250) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `tbl_message`
--

INSERT INTO `tbl_message` (`id`, `content`, `status`, `visitor_name`, `created_date`, `deleted_date`, `modified_date`) VALUES
(6, 'Taiyo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui', 1, 'Janie Jones', '2019-04-21 09:51:17', NULL, '2019-04-21 11:09:23'),
(7, 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui', 1, 'Janie Jones', '2019-04-21 09:51:23', NULL, NULL),
(10, 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui', 1, 'Janie Jones', '2019-04-21 09:51:23', NULL, NULL),
(11, 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui', 1, 'Janie Jones', '2019-04-21 09:51:17', NULL, NULL),
(12, 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui', 1, 'Janie Jones', '2019-04-21 09:51:23', NULL, NULL),
(13, 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui', 1, 'Janie Jones', '2019-04-21 09:51:17', NULL, NULL),
(14, 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui', 1, 'Janie Jones', '2019-04-21 09:51:12', NULL, NULL),
(15, 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui', 1, 'Janie Jones', '2019-04-21 09:51:23', NULL, NULL),
(16, 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui', 1, 'Janie Jones', '2019-04-21 09:51:17', NULL, NULL),
(17, 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui', 1, 'Janie Jones', '2019-04-21 09:51:23', NULL, NULL),
(18, 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui', 1, 'Janie Jones', '2019-04-21 09:51:23', NULL, NULL),
(19, 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui', 1, 'Janie Jones', '2019-04-21 09:51:23', NULL, NULL),
(20, 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui', 1, 'Janie Jones', '2019-04-21 09:51:23', NULL, NULL),
(21, 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui', 1, 'Janie Jones', '2019-04-21 09:51:23', NULL, NULL),
(22, 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui', 1, 'Janie Jones.', '2019-04-21 09:51:23', NULL, '2019-04-21 16:31:20'),
(23, 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui', 1, 'Taiyo Nguyễn', '2019-04-21 16:23:56', NULL, '2019-04-21 16:28:21'),
(24, 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui', 1, 'Taiyo Nguyễn', '2019-04-21 20:56:13', NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
