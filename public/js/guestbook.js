var page = 1;
$(document).ready(function () {
    init();
    $('#btn_post').click(function () {
        getMessageForm();
    });
    $('.login_link a').click(function () {
        loginForm();
    });
    if (localStorage.getItem("INFO_LOGIN")) {
        $('.wrap_icon_action').show();
        $('.login_link').html('');
        $('.login_link').append('Admin logged - <span onClick="doLogOut();" class="blue_line">Logout</span>');
    }
});
function loginForm() {
    showPopup({
        title: "Admin Login",
        content: '<form role="form" id="login_form"><div class="form-group"> <i >Using admin account <br />(username:admin - password:123456) to login.</i>  </div> <div class="form-group"> <label ><span class="glyphicon glyphicon-eye-open"></span> User Name:</label> <input value="admin" name="info[user_name]" type="text" class="form-control" > </div><div class="form-group"> <label for="usrname"><span class="glyphicon glyphicon-user"></span> Password:</label> <input value="123456"  name="info[password]" type="password" class="form-control" > </div> </form>',
        buttons: {
            c: {dismiss: true, class: 'left', text: 'Cancel'},
            s: {
                class: 'right', text: 'Login', func: 'doLogin();'
            },
        },
        width: "450px"
    });
}
function doLogin() {
    var user_name = $('input[name="info[user_name]"]').val();
    var password = $('input[name="info[password]"]').val();
    if (user_name !== 'admin' || password !== '123456') {
        $.notify("Username or password invalid", "error");
        return;
    }
    localStorage.setItem("INFO_LOGIN", "admin");
    $.notify("Login successfully", "success");
    $('.login_link').html('');
    $('.login_link').append('Admin logged - <span onClick="doLogOut();" class="blue_line">Logout</span>');
    hidePopup();
    if (localStorage.getItem("INFO_LOGIN")) {
        $('.wrap_icon_action').show();
    }
}
function doLogOut() {
    localStorage.setItem("INFO_LOGIN", "");
    $.notify("Logout successfully", "success");
    setTimeout(function () {
        location.reload();
    }, 1000);

}
function generatePaging($pages, $current_page) {
    if ($(".pagination li").length === 0) {
        var paging = '<ul class="pagination">';
        paging += '<li class="page-item prev_page"><a class="page-link"> < </a></li>';
        for (var i = 1; i <= $pages; i++) {
            if ($current_page === i) {
                paging += '<li id="' + i + '" class="page-item"><a class="page-link active"> ' + i + ' </a></li>';
            } else {
                paging += '<li id="' + i + '" class="page-item"><a class="page-link"> ' + i + ' </a></li>';
            }
        }
        paging += '<li class="page-item next_page"><a class="page-link"> > </a></li>';
        paging += '</ul>';
        $('.wrap_paging').append(paging);

        $(".pagination li").on('click', function (e) {
            e.preventDefault();
//            $("#target-content").html('loading...');
            var current = this;
            if ($(this).hasClass('prev_page')) {
                current = $('a.page-link.active').parent().prev();
            }
            if ($(this).hasClass('next_page')) {
                current = $('a.page-link.active').parent().next();
            }
            page = $(current).attr('id');
            $(".pagination li a").removeClass('active');
            $(current).find('a').addClass('active');
            init();
        });
    }
    $('.prev_page').hide();
    if ($current_page != 1) {
        $('.prev_page').show();
    }
    $('.next_page').hide();
    if ($current_page != $pages) {
        $('.next_page').show();
    }

}
function validateMessageForm() {
    var signature = $('input[name="info[visitor_name]"]').val();
    var content_message = $('input[name="info[content]"]').val();
    if (signature === "" || content_message === "") {
        $.notify("Name and message fields are required!", "error");
        return false;
    }
    return true;
}
function editMessageForm($obj) {
    var item = $($obj).closest('.item_msg');
    var data = item.find('.content_mesage').data();
    var content_message = item.find('.content_mesage').text();
    var author = item.find('.signature').text();
    showPopup({
        title: "Edit message",
        content: '<form role="form" id="message_form"> <div class="form-group"> <label ><span class="glyphicon glyphicon-eye-open"></span> Name:</label> <input value="' + author + '" name="info[visitor_name]" type="text" class="form-control"  placeholder="Enter your name"> </div><div class="form-group"> <label for="usrname"><span class="glyphicon glyphicon-user"></span> Message:</label> <textarea name="info[content]" placeholder="leave a messsage" class="form-control">' + content_message + '</textarea> </div><input type="hidden" name="info[id]" value=' + data.id + ' />  <input type="hidden" name="info[modified_date]" value="" />  <input type="hidden" name="action" value="edit" />  </form>',
        buttons: {
            c: {dismiss: true, class: 'left', text: 'Cancel'},
            s: {
                class: 'right', text: 'Edit', func: 'edit(' + data.id + ');'
            },
        },
        width: "450px"
    });
}
function edit($id) {
    var valid = validateMessageForm();
    if (!valid) {
        return;
    }
    $('input[name="info[modified_date]"]').val(getCurrentFormatedTime());
    $.ajax({
        type: 'POST',
        url: 'guestBook.php',
        data: $('#message_form').serialize(),
        dataType: 'json',
        success: function (result) {
            if (result.status === 1) {
                $.notify("Message is modified successfully", "success");
                hidePopup();
                init();
            } else {
                $.notify("An error occurred during processing, please try it again!", "error");
            }
        }
    });
}
function confirmPopup($obj) {
    var obj = $obj;
    var content = obj.content;
    var func = obj.func;
    var title = obj.title;
    var width = obj.width || "450px";
    showPopup({
        title: title,
        content: content,
        buttons: {
            c: {dismiss: true, class: 'left', text: 'Cancel'},
            s: {
                dismiss: true, class: 'right', text: 'OK', func: func
            },
        },
        width: width
    });
}
function confirmDeleteMessage($id) {
    confirmPopup({
        title: "Confirmation",
        content: "<p>Are you sure to delete this message?</p>",
        func: 'deleteMessage(' + $id + ')',
        width: "300px"
    });
}
function deleteMessage($id) {
    if (!$id) {
        return;
    }
    $.ajax({
        type: 'POST',
        url: 'guestBook.php',
        data: {
            id: $id,
            action: 'delete'
        },
        dataType: 'json',
        success: function (result) {
            if (result.status === 1) {
                $.notify("Message is Deleted successfully", "success");
                init();
            } else {
                $.notify("An error occurred during processing, please try it again!", "error");
            }
        }
    });
}
function init() {
    $.ajax({
        type: 'GET',
        url: 'guestBook.php',
        data: {
            action: "get",
            page: page
        },
        dataType: 'json',
        success: function (result) {
            var pages = result.pages;
            var data = result.data;
            generatePaging(pages.no_of_pages, pages.current_page);
            generateItem(data);

        }
    });
}
function generateItem($data) {
    var data = $data;
    if (data.length > 0) {
        var str = '';
        $('.wrap_item').html("");
        for (var i = 0; i < data.length; i++) {
            str += '<div class="col-sm-6 col-lg-6 item_msg">';
            str += '<p class="content_mesage" data-id="' + data[i].id + '">' + data[i].content + '</p>';
            str += '<div class="row">';
            str += '<div class="col-sm-8 col-lg-8 col-8">';
            str += ' <p class="signature">' + data[i].visitor_name + '</p>';
            str += '<small>' + data[i].created_date + '</small></div>';
            str += '<div class="col-sm-4 col-lg-4 col-4 wrap_icon_action no-padding dn">';
            str += '<a onClick="editMessageForm($(this));"> <i class="fas fa-pencil-alt"></i> </a>&nbsp;';
            str += '<a onClick="confirmDeleteMessage(' + data[i].id + ')"> <i class="fas fa-trash"></i> </a>';
            str += '</div></div></div>';
        }
        $('.wrap_item').append(str);
        if (localStorage.getItem("INFO_LOGIN")) {
            $('.wrap_icon_action').show();
        }
        hideLoading();
    }
}
function getMessageForm() {
    showPopup({
        title: "Enter message",
        content: '<form role="form" id="message_form"> <div class="form-group"> <label ><span class="glyphicon glyphicon-eye-open"></span> Name:</label> <input name="info[visitor_name]" type="text" class="form-control"  placeholder="Enter your name"> </div><div class="form-group"> <label for="usrname"><span class="glyphicon glyphicon-user"></span> Message:</label> <textarea name="info[content]" placeholder="leave a messsage" class="form-control"></textarea> </div><input type="hidden" name="info[status]" value=1 />  <input type="hidden" name="info[created_date]" value="" />  <input type="hidden" name="action" value="save" />  </form>',
        buttons: {
            c: {dismiss: true, class: 'left', text: 'Cancel'},
            s: {
                class: 'right', text: 'Post', func: 'submitMessage();'
            },
        },
        width: "450px"
    });
}
function submitMessage() {
    var valid = validateMessageForm();
    if (!valid) {
        return;
    }
    $('input[name="info[created_date]"]').val(getCurrentFormatedTime());
    $.ajax({
        type: 'POST',
        url: 'guestBook.php',
        data: $('#message_form').serialize(),
        dataType: 'json',
        success: function (result) {
            if (result.status === 1) {
                $.notify("Posting message successfully", "success");
                hidePopup();
                init();
            } else {
                $.notify("An error occurred during processing, please try it again!", "error");
            }
        }
    });
}
function getCurrentFormatedTime() {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date + ' ' + time;
    return dateTime;
}
function hidePopup($el) {
    $($el).parent('.modal-popup').modal('hide');
    $("#wrap_popup").find('.modal-popup').modal('hide');
}
function showPopup(obj) {
    var popup = $('#wrap_popup').html();
    var title = obj.title || "";
    var content = obj.content || "";
    var buttons = obj.buttons || "";
    var width = obj.width || "400px;";
    $('#wrap_popup').find('.modal-title').html(title);
    $('#wrap_popup').find('.modal-body').html(content);
//    $('#wrap_popup').find('.modal-dialog').css({width: width});
    if (buttons) {
        $('#wrap_popup').find('.modal-footer').html('');
        for (var key in buttons) {
            var classbtn = buttons[key].class || "";
            var dismiss = buttons[key].dismiss ? "modal" : "";
            var textBtn = buttons[key].text || "";
            var funcBtn = buttons[key].func || "";
            if (key === 'c') {
                classbtn += ' mr-auto';
            }
            var btn = '<button onClick="' + funcBtn + '" type="button" class="' + classbtn + '" data-dismiss="' + dismiss + '">' + textBtn + '</button>';
            $('#wrap_popup').find('.modal-footer').append(btn);
        }

    }
    $("#wrap_popup").find('.modal-popup').modal('show');
}
function showLoading() {
    $.fakeLoader({
        bgColor: '#e74c3c',
        spinner: 'spinner2',
        timeToHide: 50000
    });
}
function hideLoading() {
    $.fakeLoader({
        bgColor: '#e74c3c',
        spinner: 'spinner2',
        timeToHide: 1000
    });
}