<?php
$action = "";

if (isset($_POST['action'])) {
    $action = $_POST['action'];
}
if (isset($_GET['action'])) {
    $action = $_GET['action'];
}
if (isset($action)) {
    switch ($action) {
        case 'save':
            save();
            break;
        case 'edit':
            edit();
            break;
        case 'delete':
            delete();
            break;
        default:
            get();
            break;
    }
}

function save()
{
    if (isset($_POST['info'])) {
        $info = $_POST['info'];
        $data = prepare($info);
        $my_Db_Connection = connect();
        $my_Insert_Statement = $my_Db_Connection->prepare("INSERT INTO tbl_message (" . $data['columns'] . ") VALUES (" . $data['params'] . ")");
        $params = explode(',', $data['params']);
        $values = $data['value'];
        for ($i = 0; $i < count($params); $i++) {
            $my_Insert_Statement->bindParam($params[$i], $values[$i]);
        }
        if ($my_Insert_Statement->execute()) {
            echo json_encode(array('status' => 1));
            exit();
        }
    }
    echo json_encode(array('status' => 0));
}

function edit()
{
    if (isset($_POST['info'])) {
        $info = $_POST['info'];
        $db = connect();
        $sql = "UPDATE tbl_message SET content=:content, visitor_name=:visitor_name, modified_date=:modified_date WHERE id=:id";
        $statement = $db->prepare($sql);
        if ($statement->execute($info)) {
            echo json_encode(array('status' => 1));
            exit();
        }
    }
    echo json_encode(array('status' => 0));
}

function delete()
{
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
        $db = connect();
        $statement = $db->prepare("DELETE FROM tbl_message WHERE id = :id");
        $statement->bindParam(':id', $id);
        if ($statement->execute()) {
            echo json_encode(array('status' => 1));
            exit();
        }
    }
    echo json_encode(array('status' => 0));
}

function get()
{
    $from = 0;
    $to = 6;
    $items_per_page = 6;
    $current_page = 1;
    $db = connect();
    if (isset($_GET['page'])) {
        if ($_GET['page'] != 1) {
            $current_page = $page = $_GET['page'];
            $from = ($page - 1) * $items_per_page;
//            $to = $from + $items_per_page;
        }
    }

    $sql_count = 'SELECT count(*) as total FROM tbl_message WHERE status = 1';
    $count = $db->query($sql_count)->fetch(PDO::FETCH_ASSOC);
    $total = $count['total'];
    $no_of_page = ceil($total / $items_per_page);
    $sql = 'SELECT id, content, visitor_name, created_date, modified_date FROM tbl_message WHERE status = 1 order by id DESC limit ' . $from . ',' . $items_per_page . '';
    $data = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    if (count($data) > 0) {
        $data = editForView($data);
    }
    echo json_encode(array('data' => $data, 'pages' => array('no_of_pages' => $no_of_page, 'current_page' => $current_page)));
}

function editForView($data)
{
    foreach ($data as &$items) {
        if ($items['modified_date']) {
            $items['created_date'] = $items['modified_date'];
        }
        foreach ($items as $key => $item) {

            if ($key === 'created_date') {
                $date = date('dS M, Y', strtotime($item));
                $time = str_replace(' ', '', date('h:i a', strtotime($item)));
                $items[$key] = $date . ' at ' . $time;
            }
        }
    }
    return $data;
}

function prepare($data)
{
    $columns = "";
    $value = [];
    $params = "";
    foreach ($data as $key => $val) {
        $columns .= $key . ",";
        $params .= ":" . $key . ",";
        $value[] = $val;
    }
    return array('columns' => rtrim($columns, ','), 'value' => $value, 'params' => rtrim($params, ','));
}

function connect()
{
//
//    $mysqli = new mysqli("localhost", "root", "", "guestbook");
//    $mysqli->set_charset('utf8');
//    // Check connection
//    if ($mysqli->connect_errno) {
//        echo 'Connect failed: ' . $mysqli->connect_error;
//        exit();
//    }
//    return $mysqli;
    $servername = "localhost";
    $database = "guestbook";
    $username = "root";
    $password = "";
    $sql = "mysql:host=$servername;dbname=$database;";
    $dsn_Options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"];
    try {
        $my_Db_Connection = new PDO($sql, $username, $password, $dsn_Options);
    } catch (PDOException $error) {
        echo 'Connection error: ' . $error->getMessage();
    }
    return $my_Db_Connection;
}

?>