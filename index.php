<!DOCTYPE HTML>
<html>
    <head>
        <title>Guestbook</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="public/library/jquery/jquery-3.4.0.min.js"></script>
        <link rel="stylesheet" type="text/css" href="public/library/bootstrap4/css/bootstrap.min.css">
        <script src="public/library/bootstrap4/js/bootstrap.min.js"></script>
        <script src="public/library/notify/notify.min"></script>
        <link href="public/library/loading/css/fakeLoader.min.css" rel="stylesheet" type="text/css"/>
        <script src="public/library/loading/js/fakeLoader.min.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="public/library/fontawesome5/css/all.min.css">
        <link rel="stylesheet" type="text/css" href="public/css/style.css">
        <script src="public/js/guestbook.js"></script>
    </head>
    <body>
        <!--Loading-->
        <div class="fakeLoader"></div>
        <section class="bg-site">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 col-lg-3 col-12 col-left">
                        <img src="public/images/logo.png" class="mx-auto d-block img-responsive" />
                        <div class="col-sm-12"><hr class="col-sm-6 col-lg-6"/></div>
                        <h2>Guestbook</h2>
                        <p class="intro_service">Feel free to leave us a short message to tell us what you think to our services</p>
                        <button id="btn_post" class="btn_post">Post a Message</button>
                        <p class="login_link"><a>Login</a></p>
                    </div>
                    <div class="col-sm-9 col-lg-9 col-12 col-right">
                        <div class="container">
                            <div class="row wrap_item"></div>
                            <div class="row">
                                <div class="col-sm-12 col-lg-12">
                                    <div class="d-flex justify-content-center wrap_paging"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Loading-->
        <div class="fakeLoader"></div>
        <!-- The Modal -->
        <div id="wrap_popup">
            <div class="modal modal-popup fade" id="myModal">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"></h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body"></div>
                        <div class="modal-footer"></div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
